package com.alexlitovsky.bugs.quarkus.compression;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

@WebServlet(name="test-servlet", urlPatterns="/test/*")
public class TestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private String json;

	@Override
	public void init() throws ServletException {
		try {
			try (InputStream inpusStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("/test.json")) {
				json = IOUtils.toString(inpusStream, "UTF-8");
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		resp.setStatus(200);
		resp.getWriter().write(json);
	}
}
